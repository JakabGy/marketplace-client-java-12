package com.atlassian.marketplace.client.encoding;

/**
 * Indicates that an object returned by the server (or constructed by the caller) was invalid
 * because a field had a string value that was not one of its allowable values.
 * @since 2.0.0
 */
public class InvalidFieldValue extends SchemaViolation
{
    private final String value;
    
    public InvalidFieldValue(String value, Class<?> valueClass)
    {
        super(valueClass);
        this.value = value;
    }
    
    @Override
    public String getMessage()
    {
        return "\"" + value + "\" is not a valid value for " + getSchemaClass().getSimpleName();
    }
    
    /**
     * The specific string value that caused the error.
     */
    public String getValue()
    {
        return value;
    }
}
