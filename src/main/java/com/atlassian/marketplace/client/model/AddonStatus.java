package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.EnumWithKey;

/**
 * Indicates whether an {@link Addon} is publicly visible, private, or pending approval.
 * @since 2.0.0
 */
public enum AddonStatus implements EnumWithKey
{
    /**
     * The add-on is visible to everyone.
     */
    PUBLIC("public"),
    /**
     * The add-on is visible only to users associated with its vendor; it can also be installed
     * by anyone who has received an access token from the vendor.
     */
    PRIVATE("private"),
    /**
     * The add-on is pending approval, and will become public once it has been approved.
     */
    SUBMITTED("submitted"),
    /**
     * The add-on was submitted for approval, but was rejected. It can be resubmitted for approval
     * by setting its status back to {@link #SUBMITTED} (see
     * {@link com.atlassian.marketplace.client.api.Addons#updateAddon(Addon, Addon)}).
     */
    REJECTED("rejected");
    
    private final String key;
    
    private AddonStatus(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
