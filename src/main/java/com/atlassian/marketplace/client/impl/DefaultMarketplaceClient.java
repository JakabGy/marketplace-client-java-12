package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonCategories;
import com.atlassian.marketplace.client.api.Addons;
import com.atlassian.marketplace.client.api.Applications;
import com.atlassian.marketplace.client.api.Assets;
import com.atlassian.marketplace.client.api.LicenseTypes;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.Products;
import com.atlassian.marketplace.client.api.Vendors;
import com.atlassian.marketplace.client.http.HttpConfiguration;
import com.atlassian.marketplace.client.http.HttpTransport;
import com.atlassian.marketplace.client.model.Links;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Default implementation of {@link MarketplaceClient} for the Marketplace 2.0 API.
 */
public final class DefaultMarketplaceClient implements MarketplaceClient
{
    public static final URI DEFAULT_SERVER_URI = URI.create("https://marketplace.atlassian.com");
    
    protected final URI baseUri;
    protected final HttpTransport httpTransport;
    protected final EntityEncoding encoding;
    
    public static final String API_VERSION = "2";
    
    private final ApiHelper apiHelper;
    
    /**
     * Constructs a {@link DefaultMarketplaceClient} using the default HTTP implementation.
     * 
     * @param baseUri  The root URI of the Marketplace server.
     * @param configuration  Client parameters such as timeouts, authentication, and proxy settings.
     */
    public DefaultMarketplaceClient(URI baseUri, HttpConfiguration configuration)
    {
        this(baseUri, new CommonsHttpTransport(configuration, baseUri), new JsonEntityEncoding());
    }
    
    /**
     * Constructs a {@link DefaultMarketplaceClient} using a specific HTTP implementation and
     * response parser.
     * 
     * @param serverBaseUri  The root URI of the Marketplace server.
     * @param httpTransport  An object that will execute all HTTP requests.
     * @param encoding  An {@link EntityEncoding} implementation.
     */
    public DefaultMarketplaceClient(URI serverBaseUri, HttpTransport httpTransport, EntityEncoding encoding)
    {
        this.baseUri = ApiHelper.normalizeBaseUri(checkNotNull(serverBaseUri, "serverBaseUri"))
            .resolve("rest/" + API_VERSION + "/");
        this.httpTransport = checkNotNull(httpTransport, "httpTransport");
        this.encoding = encoding;

        this.apiHelper = new ApiHelper(this.baseUri, httpTransport, encoding);
    }

    @Override
    public void close()
    {
        try
        {
            httpTransport.close();
        }
        catch (IOException e)
        {
        }
    }
    
    @Override
    public boolean isReachable()
    {
        return apiHelper.checkReachable(baseUri);
    }

    @Override
    public Links getRootLinks() throws MpacException
    {
        return getRoot().getLinks();
    }
    
    @Override
    public HttpTransport getHttp()
    {
        return httpTransport;
    }
    
    @Override
    public <T> String toJson(T entity) throws MpacException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        encoding.encode(os, entity, true);
        return new String(os.toByteArray());
    }
    
    @Override
    public Addons addons() throws MpacException
    {
        return new AddonsImpl(apiHelper, getRoot());
    }

    @Override
    public AddonCategories addonCategories() throws MpacException
    {
        return new AddonCategoriesImpl(apiHelper, applications());
    }
    
    @Override
    public Applications applications() throws MpacException
    {
        return new ApplicationsImpl(apiHelper, getRoot());
    }

    @Override
    public Assets assets() throws MpacException
    {
        return new AssetsImpl(apiHelper, getRoot());
    }

    @Override
    public LicenseTypes licenseTypes() throws MpacException
    {
        return new LicenseTypesImpl(apiHelper, getRoot());
    }

    @Override
    public Products products() throws MpacException
    {
        return new ProductsImpl(apiHelper, getRoot());
    }

    @Override
    public Vendors vendors() throws MpacException
    {
        return new VendorsImpl(apiHelper, getRoot());
    }
    
    @Override
    public <T> Page<T> getMore(PageReference<T> ref) throws MpacException
    {
        return apiHelper.getMore(ref);
    }
    
    InternalModel.MinimalLinks getRoot() throws MpacException
    {
        return apiHelper.getEntity(baseUri, InternalModel.MinimalLinks.class);
    }
}
