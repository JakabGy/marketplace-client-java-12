/**
 * Interfaces and classes related to the HTTP transport layer.
 * @since 2.0.0
 */
package com.atlassian.marketplace.client.http;