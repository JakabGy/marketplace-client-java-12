package com.atlassian.marketplace.client.api;

import java.util.Optional;

import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.atlassian.marketplace.client.api.QueryProperties.describeValues;
import static com.atlassian.marketplace.client.util.Convert.iterableOf;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.empty;

/**
 * Encapsulates parameters that can be passed to {@link Applications#safeGetVersion}.
 * @since 2.0.0
 */
public final class ApplicationVersionsQuery implements QueryProperties.Bounds, QueryProperties.Hosting
{
    private static final ApplicationVersionsQuery DEFAULT_QUERY = builder().build();
    
    private final Optional<Integer> afterBuildNumber;
    private final Optional<HostingType> hosting;
    private final QueryBounds bounds;

    /**
     * Returns a new {@link Builder} for constructing an ApplicationVersionsQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns an ApplicationsQuery with no criteria, which will query the most recent application versions.
     */
    public static ApplicationVersionsQuery any()
    {
        return DEFAULT_QUERY;
    }
    
    /**
     * Returns a new {@link Builder} for constructing an ApplicationVersionsQuery based on an existing ApplicationVersionsQuery.
     */
    public static Builder builder(ApplicationVersionsQuery query)
    {
        Builder builder = builder()
            .afterBuildNumber(query.safeGetAfterBuildNumber())
            .hosting(query.safeGetHosting())
            .bounds(query.getBounds());

        return builder;
    }
    
    private ApplicationVersionsQuery(Builder builder)
    {
        afterBuildNumber = builder.afterBuildNumber;
        hosting = builder.hosting;
        bounds = builder.bounds;
    }

    /**
     * The version build number, if any, that the client has specified in order to search for
     * later versions.
     * @see Builder#afterBuildNumber(Optional)
     */
    public Optional<Integer> safeGetAfterBuildNumber()
    {
        return afterBuildNumber;
    }

    @Override
    public Optional<HostingType> safeGetHosting()
    {
        return hosting;
    }

    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("ApplicationVersionsQuery",
            describeValues("afterBuildNumber", iterableOf(afterBuildNumber)),
            describeValues("hosting", iterableOf(hosting)),
            bounds.describe()
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof ApplicationVersionsQuery) ? toString().equals(other.toString()) : false;
    }
    
    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    
    /**
     * Builder class for {@link ApplicationVersionsQuery}.  Use {@link ApplicationVersionsQuery#builder()} to create an instance. 
     */
    public static class Builder implements QueryBuilderProperties.Bounds<Builder>, QueryBuilderProperties.Hosting<Builder>
    {
        private Optional<Integer> afterBuildNumber = empty();
        private Optional<HostingType> hosting = empty();
        private QueryBounds bounds = QueryBounds.defaultBounds();

        /**
         * Returns an immutable {@link ApplicationVersionsQuery} based on the current builder properties.
         */
        public ApplicationVersionsQuery build()
        {
            return new ApplicationVersionsQuery(this);
        }

        /**
         * Specifies an existing version if you want to query versions that are later than that
         * version.
         * @param afterBuildNumber  the build number of an existing application version
         *   to restrict the query to versions later than this version, or {@link Optional#empty()} to
         *   remove any such restriction 
         * @return  the same query builder
         * @see ApplicationVersionsQuery#safeGetAfterBuildNumber()
         */
        public Builder afterBuildNumber(Optional<Integer> afterBuildNumber)
        {
            this.afterBuildNumber = checkNotNull(afterBuildNumber);
            return this;
        }

        @Override
        public Builder hosting(Optional<HostingType> hosting)
        {
            this.hosting = checkNotNull(hosting);
            return this;
        }

        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
    }
}
